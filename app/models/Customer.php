<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 02/01/2016
 * Time: 19:06
 */
use \Phalcon\Mvc\Model;

use \Curl\Curl;

class Customer extends Model
{
    protected $id;
    protected $firstname;
    protected $lastname;
    protected $address;
    protected $gender;
    protected $plate_number;
    protected $telephone;
    protected $serial_number;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getPlateNumber()
    {
        return $this->plate_number;
    }

    /**
     * @param mixed $plate_number
     */
    public function setPlateNumber($plate_number)
    {
        $this->plate_number = $plate_number;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        return $this->serial_number;
    }

    /**
     * @param mixed $serial_number
     */
    public function setSerialNumber($serial_number)
    {
        $this->serial_number = $serial_number;
    }


    public function persistRegistrationIssues($customer)
    {


    }


    public function getTransactionBySerial($serial_number)
    {

        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);

        $sql = "SELECT * FROM sanwo_new.`credit_transaction` ct WHERE ct.`serial_number` = '{$serial_number}'";
        $result = $db->objectBuilder()->rawQueryOne($sql);

        return $result;
    }

}