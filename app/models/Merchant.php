<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 06/01/2016
 * Time: 10:31
 */
use \Phalcon\Mvc\Model;

class Merchant extends Model
{
    private $db;


    public function getCashiers($merchant_id)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);

        $query = "SELECT u.`email`, u.`telephone`, p.`firstname`, p.`user_id`,
p.`lastname`, p.`address`, mc.`cashier_id`, mc.`merchant_id`, u.`id` AS 'user_id'

 FROM sanwo_new.`merchant_cashier` mc  JOIN
 sanwo_new.`user` u ON u.`id` = mc.`cashier_id` JOIN
  sanwo_new.`profile` p ON p.`user_id` = u.`id`
   WHERE mc.`merchant_id` = {$merchant_id}";


        $result = $db->objectBuilder()->rawQuery($query);

        return $result;


    }


    public function getDevices($merchant_id)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);
        $query = "  SELECT d.`device_code`, d.`id`, d.`address`,  md.`merchant_id`  FROM sanwo_new.`merchant_device` md JOIN sanwo_new.`device` d ON
   d.`id` = md.`device_id` WHERE md.`merchant_id` = {$merchant_id};";

        $result = $db->objectBuilder()->rawQuery($query);

        return $result;
    }


    public function getSyncedTransaction($check_history_id)
    {
        try {
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            $db = new MysqliDb($mysqli);
            $sql = "SELECT * FROM sanwo_new.`credit_transaction` ct
                    JOIN sanwo_new.`profile` p ON p.`user_id` = ct.`holder_id`
                    WHERE ct.`check_history_id` = {$check_history_id};";

            $result = $db->objectBuilder()->rawQuery($sql);
            return $result;

        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }


    public function getCashierTransaction($cashier_id)
    {
        try {
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            $db = new MysqliDb($mysqli);
            $sql = "SELECT * FROM sanwo_new.`credit_transaction` ct WHERE ct.`holder_id` = {$cashier_id};";

            $result = $db->objectBuilder()->rawQuery($sql);
            return $result;

        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }


}