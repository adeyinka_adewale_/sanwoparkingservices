<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 11/01/2016
 * Time: 00:10
 */

use \Curl\Curl;

class SmsClient
{
    const USERNAME = 'flbolamide@yahoo.com';//store in environ variables
    const PASSWORD = 'orangecow';//store in environ variables
    const API_URL = 'http://sms.bbnplace.com/bulksms/bulksms.php?username={{username}}&password={{password}}&sender={{sender}}&message={{message}}&mobile={{mobile}}';

    const DEFAULT_SENDER = 'OGSPMS';
    const SUCCESS_RESPONSE = '1801';
    const MESSAGE = "'Dear {{name}},
Welcome to OGSPMS parking card. Your card is now activated for use based on the details you provided, Thanks for supporting the intiative';
";

    public static function send($to, $msg, $params = array(), $from = self::DEFAULT_SENDER)
    {
        $url = self::prepareAPI($to, $msg, $from, self::USERNAME, self::PASSWORD, $params);

        $c = new Curl();
        return $c->get($url);

    }

    protected static function prepareAPI($to, $msg, $from, $username, $password, $params)
    {
        $msg = self::replaceValues($msg, $params);

        $url = str_replace('{{username}}', $username, self::API_URL);
        $url = str_replace('{{password}}', $password, $url);
        $url = str_replace('{{sender}}', $from, $url);
        $url = str_replace('{{mobile}}', $to, $url);
        $url = str_replace('{{message}}', urlencode($msg), $url);

        return $url;
    }

    protected static function replaceValues($message, $params)
    {
        foreach ($params as $key => $value) {
            $message = str_replace('{{' . $key . '}}', $value, $message);
        }
        return $message;
    }
}