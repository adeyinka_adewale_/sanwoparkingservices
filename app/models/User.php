<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 31/12/2015
 * Time: 21:08
 */
use \Curl\Curl;


class User
{

    public $email;
    public $access_token;
    public $password;
    protected $id;
    protected $firstname;
    protected $lastname;
    protected $address;
    protected $gender;
    protected $plate_number;
    protected $telephone;
    protected $serial_number;

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        return $this->serial_number;
    }

    /**
     * @param mixed $serial_number
     */
    public function setSerialNumber($serial_number)
    {
        $this->serial_number = $serial_number;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getPlateNumber()
    {
        return $this->plate_number;
    }

    /**
     * @param mixed $plate_number
     */
    public function setPlateNumber($plate_number)
    {
        $this->plate_number = $plate_number;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }


    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @param mixed $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    //login user

    public function login($identifier, $password)
    {

        $url = 'http://46.101.56.210/sanwo-service/public/user/login';
        $data = array(
            'identifier' => $identifier,
            'password' => $password
        );
        $curl = new Curl();
        $curl->post($url, $data);
        return $curl;

    }

    public function register($data, $token, $client_id)
    {
        //$url = '/customer/register';
        //  $url = 'http://54.68.183.75/sanwo-service/customer/register';
        $url = 'http://accounts-sanwo.cloudapp.net/customer/register';
        //post payload from client
        $data = json_decode(file_get_contents('php://input'), true);

        //print_r($data);
        $curl = new Curl();

        $curl->setHeader('a', $token);
        $curl->setHeader('i', $client_id);

        //create the response object
        $response = array(

            'success' => 0,
            'message' => '',
            'status' => null,
            'failed' => 0,
            'success_cards' => array(),
            'failed_cards' => array()

        );


        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => 'MYSQL5008.Smarterasp.net',
            'username' => '9aaf65_parking',
            'password' => '123456789',
            'dbname' => 'db_9aaf65_parking',
        ));

        $count = 0;

        if (isset($data)) {
            foreach ($data as $key => $item) {

                // echo $item['serial_number'] . '<br />';
                $result = $curl->post($url, array(
                    'firstname' => $item['firstname'],
                    'lastname' => $item['lastname'],
                    'serial_number' => $item['serial_number'],
                    'gender' => 0,
                    'email' => $item['plate_number'] . '@sanwo.me',
                    'telephone' => $item['telephone']
                ));


                if ($curl->httpStatusCode == 200) {

                    //var_dump($curl->response);
                    $object_type = gettype($curl->response);

                    $response_object = null;

                    if ($object_type == 'string') {

                        $response_object = json_decode($curl->response);
                    } else {
                        $response_object = json_decode(json_encode($curl->response));
                    }


                    //  var_dump($response_object);

                    if ($response_object->status == 1) {
                        $count += 1;
                        $response['success'] = $count;
                        $response['status'] = 1;
                        $response['message'] = 'Customers successfully registered';
                        array_push($response['success_cards'], $item['serial_number']);
                        // return $response;

                    } else {

                        // var_dump($response_object);
                        $response['failed'] += 1;
                        $response['message'] = $response_object->message;
                        $response['status'] = 2;
                        array_push($response['failed_cards'], $item['serial_number']);
                        $query = "INSERT INTO REGISTRATION (`id`, `firstname`, `lastname`, `plate_number`, `phone_number`, `status`, `created_at`, `modified_at`, `registered_by`, `access_token`, `issuer_id`, `serial_number`, `error_message`)

                VALUES (0, '{$item['firstname']}', '{$item['lastname']}',
                '{$item['plate_number']}', '{$item['telephone']}', 2, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP , $client_id, '{$token}', 10, '{$item['serial_number']}', '{$response['message']}')";

                        $result = $connection->execute($query);



                    }


                } else {
                    return json_encode($curl->response);
                }


            }

            return $response;
        } else
            return 'Required Parameter(s) not sent';


    }

    public function persistRegistrationIssues($customers)
    {

        foreach ($customers as $customer) {

        }


    }

    public function pollTransaction($last_id)
    {

        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);
        $response = array(
            'status' => 0,
            'data' => null
        );


        //$platenumber = 'amadebanjo@student.lautech.edu.ng';
        try {
            $result = $db->objectBuilder()->rawQuery("SELECT p.`firstname`, p.`lastname`, d.`address`, ct.`created_time`, ct.`amount`, ct.`card_balance` FROM sanwo_new.`credit_transaction` ct JOIN sanwo_new.`device` d ON d.`id` = ct.`device_id` JOIN sanwo_new.`card`
c ON c.`id` = ct.`card_id` JOIN sanwo_new.`profile` p ON p.`id` = c.`customer_id` WHERE d.`device_type_id` = 2
AND ct.`id` > 1000 AND DATE(ct.`created_time`) BETWEEN '2016-01-01' AND '2016-01-15' AND d.`issuer_id` = 10");

            return $result;

        } catch (Exception $ex) {
            $response['status'] = 3;
            $response['data'] = $ex->getMessage();


        }
        return $response;

    }

    public function search_by_plate($platenumber)
    {

        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);
        $response = array(
            'status' => 0,
            'data' => null
        );


        //$platenumber = 'amadebanjo@student.lautech.edu.ng';
        try {
            $result = $db->rawQueryOne("SELECT p.`firstname`, p.`lastname`, p.`address`, p.`phone_number` , d.id,  ct.`amount`, MAX(ct.`created_time`) AS 'parking_time', d.`address` AS \"device_address\"  FROM sanwo_new.`profile` p JOIN
sanwo_new.`user` customer ON customer.`id` = p.`user_id` JOIN sanwo_new.`card` card ON card.`customer_id` = customer.`id`
JOIN sanwo_new.`credit_transaction` ct ON ct.`card_id` = card.`id`
JOIN sanwo_new.`device` d ON d.`id` = ct.`device_id`
WHERE customer.`email` = '{$platenumber}@sanwo.me'");

            if ($db->count > 0 && $result['firstname'] != null) {

                $response['status'] = 1;
                $response['data'] = $result;
            } else {
                $response['status'] = 2;
                $response['data'] = 'Car not registered';
            }

        } catch (Exception $ex) {
            $response['status'] = 3;
            $response['data'] = $ex->getMessage();

        }
        return $response;


    }


    public function latestTransactions()
    {

        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);


    }


    public function syncRegistration($client_id)
    {

        $data = json_decode(file_get_contents('php://input'), true);

        var_dump($data);

        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'sanwo_parking',
        ));

        if (isset($data)) {

            $connection->connect();

            foreach ($data as $key => $item) {

                $query = "INSERT INTO REGISTRATION (`id`, `firstname`, `lastname`, `plate_number`, `phone_number`, `status`, `created_at`, `modified_at`, `registered_by`,`issuer_id`, `serial_number`)

                VALUES (0, '{$item['firstname']}', '{$item['lastname']}',
                '{$item['plate_number']}', '{$item['telephone']}', 2, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP , $client_id, 10, '{$item['serial_number']}')";

                $result = $connection->execute($query);


            }

        }

    }

    public function sendSms($plate_number)
    {
        $message = "Dear {{$plate_number}},
                Welcome to OGSPMS parking card. Your card is now activated for use based on the details you provided, Thanks for supporting the intiative";

        $url = 'http://sms.bbnplace.com/bulksms/bulksms.php?username=flbolamide@yahoo.com&password=orangecow&sender=OGSPMS&message={{message}}&mobile={{mobile}}';

        $curl = new Curl();
        $result = $curl->get($url);


    }


    public function confirmUser($email, $telephone, $created_time)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);

        $sql = 'select * from sanwo_new.`user` u where u.email = {$email} AND u.telephone = {$telephone} AND DATE(u.`created_time`) = {$created_time}';

        $result = $db->objectBuilder()->rawQueryOne($sql);

        return $result;
    }





}