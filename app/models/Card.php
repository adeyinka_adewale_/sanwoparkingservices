<?php

use \Phalcon\Mvc\Model;


/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 12/01/2016
 * Time: 19:34
 */
class Card extends Model
{

    public function getCardTransaction($card_id)
    {
        try {
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            $db = new MysqliDb($mysqli);

            $sql = "SELECT * FROM sanwo_new.`credit_transaction` ct WHERE ct.`card_id` = {$card_id};";

            $result = $db->objectBuilder()->rawQuery($sql);

            return $result;
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

}