<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 01/01/2016
 * Time: 22:27
 */

use Phalcon\Mvc\Model;

class Device extends Model
{
    protected $id;
    protected $device_code;
    protected $sanwo_device_id;
    protected $charge;
    protected $created_at;
    protected $updated_at;
    protected $status;
    protected $modified_at;
    private $connection;

    public function __construct($connection)
    {
        $this->connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'sanwo_parking',
        ));
    }

    public function intialize(){

        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'sanwo_parking',
        ));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @param mixed $modified_at
     */
    public function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;
    }

    public function configureBilling($device)
    {
        $device = new Device();

        $sql = "INSERT INTO device (`id`, `device_code`, `sanwo_device_id`, `status`, `created_at`, `modified_at`, `charge`, )
                VALUES (0,
                 {$device->getDeviceCode()},
                 {$device->getSanwoDeviceId()},
                 {$device->getSanwoDeviceId()},
                 {$device->getStatus()}
                ,{$device->getCreatedAt()},
                  {$device->getcharge()}
                )
              ";

        $result = $this->connection->execute($sql);
        if ($result) {

        } else {

        }


    }

    /**
     * @return mixed
     */
    public function getDeviceCode()
    {
        return $this->device_code;
    }

    /**
     * @param mixed $device_code
     */
    public function setDeviceCode($device_code)
    {
        $this->device_code = $device_code;
    }

    /**
     * @return mixed
     */
    public function getSanwoDeviceId()
    {
        return $this->sanwo_device_id;
    }

    /**
     * @param mixed $sanwo_device_id
     */
    public function setSanwoDeviceId($sanwo_device_id)
    {
        $this->sanwo_device_id = $sanwo_device_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getcharge()
    {
        return (double)$this->charge;
    }

    /**
     * @param mixed $charge
     */
    public function setcharge($charge)
    {
        if ($charge < 0) {
            throw new \InvalidArgumentException('charge can\'t be negative');
        }
        $this->charge = $charge;
    }

    public function getBillingById($sanwo_device_id)
    {

        $sql = "SELECT * FROM device WHERE sanwo_device_id = {$sanwo_device_id} ";
        $result = $this->connection->exeute($sql);


    }





}