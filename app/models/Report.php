<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 02/01/2016
 * Time: 08:37
 */
class Report
{
    private $created_time;

    private $db;
    private $sync_time;

    public function __construct()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $this->db = new MysqliDb($mysqli);
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * @param mixed $created_time
     */
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;
    }

    /**
     * @return mixed
     */
    public function getSyncTime()
    {
        return $this->sync_time;
    }

    /**
     * @param mixed $sync_time
     */
    public function setSyncTime($sync_time)
    {
        $this->sync_time = $sync_time;
    }

    public function totalAmountProcessed($start_date, $end_date, $issuer_id)
    {

        $amount = $this->db->objectBuilder()->rawQueryOne("SELECT
                                          COUNT(ct.id) `transaction_count`,
                                          SUM(ct.amount) `revenue`
                                        FROM `credit_transaction` ct
                                        INNER JOIN device d ON (d.id = ct.`device_id`)
                                        WHERE
                                          ct.device_type_id = 2
                                          AND ct.type = 2
                                          AND d.`issuer_id` = {$issuer_id}
                                          AND DATE(ct.`created_time`) BETWEEN

                                          '{$start_date}' AND '{$end_date}'");

        return $amount;
    }


    public function totalTopupIssued($issuer_id, $start_date, $end_date)
    {
        $amount = $this->db->objectBuilder()->rawQueryOne("SELECT
                                                              COUNT(tt.id) `total_unit`,
                                                              SUM(tt.amount) `total_amount`
                                                            FROM `topup_transaction` tt
                                                            INNER JOIN device d ON (d.id = tt.`device_id`)
                                                            WHERE
                                                              tt.type = 2
                                                              AND NOT tt.agent_id IS NULL
                                                              AND d.issuer_id = {$issuer_id}
                                                              AND DATE(tt.`created_time`) BETWEEN

                                                              '{$start_date}' AND '{$end_date}'");

        return $amount;
    }


    public function totalTopupSold($issuer_id, $start_date, $end_date)
    {

        $amount = $this->db->objectBuilder()->rawQueryOne("SELECT
                      SUM(ct.amount) `total_amount`,
                      COUNT(ct.id) `transaction_count`
                    FROM `credit_transaction` ct
                    INNER JOIN device d ON (d.id = ct.`device_id`)
                    WHERE
                      ct.device_type_id = 1
                      AND ct.type = 1
                      AND d.`issuer_id` = {$issuer_id}
                      AND DATE(ct.`sync_time`) BETWEEN '{$start_date}' AND
                      '{$end_date}'");

        return $amount;
    }


    public function registeredCustomers($issuer_id, $start_date, $end_date)
    {
        $amount = $this->db->objectBuilder()->rawQueryOne("SELECT
                                                              COUNT(c.customer_id) AS `new`
                                                            FROM card c
                                                            INNER JOIN `user` u ON (c.customer_id = u.id)
                                                            WHERE
                                                              NOT c.customer_id IS NULL
                                                              AND c.issuer_id = {$issuer_id}
                                                              AND DATE(u.`created_time`) BETWEEN '{$start_date}' AND '{$end_date}'");

        return $amount;
    }



    public function totalAgentBalance()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);
        $amount = $db->rawQuery("
SELECT
  r.id,
  r.device_code,
  (t.amount - r.amount) AS `balance`,
  t.amount `inflow`,
  r.amount `outflow`
FROM
(SELECT
  d.id,
  d.`device_code`,
  SUM(ct.`amount`) `amount`
FROM device d
LEFT JOIN `credit_transaction` ct ON (ct.`device_id` = d.id)
WHERE
  ct.device_type_id = 1
  AND ct.type = 1
  AND d.`issuer_id` = 10
GROUP BY d.id) AS r
INNER JOIN (SELECT
  d.id,
  SUM(tt.amount) `amount`
FROM device d
LEFT JOIN topup_transaction tt ON (tt.`device_id` = d.id)
WHERE
  tt.type = 2
  AND NOT tt.agent_id IS NULL
  AND d.`issuer_id` = 10
GROUP BY d.id) AS t ON (t.id = r.id)
GROUP BY t.id;");

        return $amount;
    }


    public function merchantSummary($issuer_id)
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $db = new MysqliDb($mysqli);
        $amount = $db->jsonBuilder()->rawQuery("SELECT
              m.`id`,
              m.`name` `merchant_name`,
              IFNULL(COUNT(ct.id), 0) `no_of_transaction`,
              IFNULL(SUM(ct.amount), 0) `total_sales`,
              IFNULL(MAX(ct.sync_time), 'N/A') `last_sync_time`
            FROM sanwo_staging.`merchant` m
              LEFT JOIN `merchant_cashier` mc ON (mc.`merchant_id` = m.`id`)
              LEFT JOIN credit_transaction ct ON (ct.`holder_id` = mc.`cashier_id`)
              JOIN sanwo_staging.`issuer_merchant` im ON im.`merchant_id` = m.`id` AND im.`issuer_id`='{$issuer_id}'
            GROUP BY m.`id`
            ORDER BY m.`name`");

        return $amount;

    }


}