<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 05/01/2016
 * Time: 19:43
 */

uses \Phalcon\Mvc\Model;

class Registration extends Model
{

    protected $firstname;
    protected $lastname;
    protected $phone_number;
    protected $plate_number;
    protected $status;
    protected $created_at;
    protected $modified_at;

}