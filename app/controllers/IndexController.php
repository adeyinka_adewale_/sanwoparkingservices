<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 31/12/2015
 * Time: 21:00
 */

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{

    public function indexAction()
    {
         echo '<h2>Hello World</h2>';
    }

    public function testDbAction()
    {
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'sanwo_parking',
        ));

        //Reconnect
        $connection->connect();


        $query = "SELECT * FROM device ";

        $result = $connection->fetchAll($query);
        print_r($result);


    }


}