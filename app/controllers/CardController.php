<?php
use \Phalcon\Mvc\Controller;

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 12/01/2016
 * Time: 19:41
 */
class CardController extends Controller
{

    public function getCardTransactionAction()
    {
        $card_id = $this->request->getQuery('card_id');
        if (isset($card_id)) {

            $card = new Card();
            $result = $card->getCardTransaction($card_id);
            echo json_encode($result);

        } else {
            echo 'Required Parameter not sent. ';
        }

    }

}