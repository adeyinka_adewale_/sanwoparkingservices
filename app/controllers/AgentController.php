<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 04/01/2016
 * Time: 10:58
 */

use \Phalcon\Mvc\Controller;


class AgentController extends Controller
{

    public function topUpHistoryAction()
    {

        $agent_id = $this->request->getQuery('agent_id');


        $agent = new Agent();

        $result = $agent->getTopUpTransactions($agent_id);

        echo $result;
    }


    public function getCreditTransactionAction()
    {
        $agent_id = $this->request->getQuery('agent_id');
        if (isset($agent_id)) {
            $agent = new Agent();
            $result = $agent->getCreditTransaction($agent_id);
            echo json_encode($result);
        } else
            echo 'Required Parameter not sent';
    }
}