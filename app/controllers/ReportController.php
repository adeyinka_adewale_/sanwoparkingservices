<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 02/01/2016
 * Time: 08:50
 */

use \Phalcon\Mvc\Controller;

class ReportController extends Controller
{
    public function merchantTransactionAction(){
        $report = new Report();

       echo json_encode($report->totalMerchantTransaction('2015-07-05'));

    }

    public function totalAmountProcessedAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();

        $issuer_id = $this->request->getQuery('i');
        $start_date = $this->request->getQuery('s');
        $end_date = $this->request->getQuery('e');


        $report = new Report();
        $result = $report->totalAmountProcessed($start_date, $end_date, $issuer_id);

        echo json_encode($result);
    }


    public function totalTopupIssuedAction()
    {
        $issuer_id = $this->request->getQuery('i');
        $start_date = $this->request->getQuery('s');
        $end_date = $this->request->getQuery('e');
        $report = new Report();
        $result = $report->totalTopupIssued($issuer_id, $start_date, $end_date);
        echo json_encode($result);
    }

    public function totalTopupSoldAction()
    {
        $issuer_id = $this->request->getQuery('i');
        $start_date = $this->request->getQuery('s');
        $end_date = $this->request->getQuery('e');

        $report = new Report();
        $result = $report->totalTopupSold($issuer_id, $start_date, $end_date);

        echo json_encode($result);
    }

    public function registeredCustomersAction()
    {
        $issuer_id = $this->request->getQuery('i');
        $start_date = $this->request->getQuery('s');
        $end_date = $this->request->getQuery('e');

        $report = new Report();
        $result = $report->registeredCustomers($issuer_id, $start_date, $end_date);

        echo json_encode($result);
    }



    public function totalAgentBalanceAction()
    {
        $report = new Report();
        $result = $report->totalAgentBalance();

        echo json_encode($result);
    }

    public function merchantSummaryAction()
    {
        $issuer_id = $this->request->getQuery('issuer_id');

        $report = new Report();
        $result = $report->merchantSummary($issuer_id);

        echo $result;
    }
}