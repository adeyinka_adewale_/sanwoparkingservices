<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 31/12/2015
 * Time: 21:30
 */



use \Curl\Curl;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model;

class CustomerController extends Controller
{


    public function loginAction(){

       if ($this->request->isPost() == true){
           $identifier = $this->request->getPost('identifier');
           $password = $this->request->getPost('password');


           $user = new User();

           $result = $user->login($identifier, $password);


          if ($result->httpStatusCode == 200){
             return json_encode($result->response);

          }





       }
        else
        {
            $response  = new ResponseHandler(null);
            $response->setStatus('500');
            $response->setData('Required Field(s) not sent');

            echo json_encode($response);
        }



    }

   public function registerAction(){

        $user = new User();
       if ($this->request->isPost() == true) {
           //get headers
           $access_token = $this->request->getHeader('a');
           $client_id = $this->request->getHeader('i');
           $response = $user->register(null, $access_token, $client_id);

           echo json_encode($response);


       }
   }

    public function searchAction(){

        $plate_number = $this->request->getQuery('plate_number');


        //echo $plate_number;

        $user = new User();

        $user = $user->search_by_plate($plate_number);
        echo json_encode($user);
    }

    public function pollAction()
    {
        $last_id = $this->request->getQuery('last_id');


        //echo $plate_number;

        $user = new User();

        $user = $user->pollTransaction($last_id);
        echo json_encode($user);
    }


    public function syncBulkAction()
    {

        $user = new User();
        if ($this->request->isPost() == true) {
            //get headers
            //$access_token = $this->request->getHeader('a');
            $client_id = $this->request->getHeader('i');
            $response = $user->syncRegistration($client_id);

            echo json_encode($response);


        }
    }

    public function testSmsAction()
    {
        // $result = SmsClient::send('2347030881110', );
    }


    public function confirmUserAction()
    {

        $email = $this->request->getQuery('email');
        $telephone = $this->request->getQuery('telephone');
        $created_time = $this->request->getQuery('created_time');

        $user = new User();
        $result = $user->confirmUser($email, $telephone, $created_time);

        if ($result['email'] == $email . '@sanwo.me' && $result['telephone'] == $telephone) {
            echo true;
        } else
            echo false;
    }

    public function getTransactionsAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();
        $cashier_id = $this->request->getQuery('serial_number');
        if (isset($cashier_id)) {
            $customer = new Customer();
            $result = $customer->getTransactionBySerial($cashier_id);
            echo json_encode($result);
        } else
            echo 'Required Parameter not sent';

    }


}