<?php

/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 06/01/2016
 * Time: 10:34
 */
use \Phalcon\Mvc\Controller;

class MerchantController extends Controller
{


    public function getAllCashiersAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();

        $merchant_id = $this->request->getQuery('merchant_id');


        if (isset($merchant_id)) {
            $merchant = new Merchant();
            $result = $merchant->getCashiers($merchant_id);

            echo json_encode($result);

        }
    }


    public function getAllDevicesAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();

        $merchant_id = $this->request->getQuery("merchant_id");

        if (isset($merchant_id)) {
            $merchant = new Merchant();
            $result = $merchant->getDevices($merchant_id);

            echo json_encode($result);
        }
    }

    public function getTransactionByCheckIdAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();
        $sync_id = $this->request->getQuery("sync_id");

        if (isset($sync_id)) {
            $merchant = new Merchant();
            $result = $merchant->getSyncedTransaction($sync_id);
            echo json_encode($result);
        } else
            echo 'Required Parameter not sent';
    }

    public function getCashierTransactionAction()
    {
        $response = $this->response;
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();
        $cashier_id = $this->request->getQuery('cashier_id');
        if (isset($cashier_id)) {
            $merchant = new Merchant();
            $result = $merchant->getCashierTransaction($cashier_id);
            echo json_encode($result);
        } else
            echo 'Required Parameter not sent';

    }

}