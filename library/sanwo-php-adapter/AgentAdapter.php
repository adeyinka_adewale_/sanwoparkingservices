<?php


namespace SanwoPHPAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;

class AgentAdapter extends BaseAdapter {

    private function updateAgentCredit($agent_id,$type,$amount){
        return $this->request(ServiceConstant::URL_CREDIT_AGENT,array(
            'agent_id'=>$agent_id,
            'type' => $type,
            'amount' => $amount
        ),self::HTTP_POST);
    }

    
    public function debitAgent($agent_id,$amount){
        return $this->updateAgentCredit($agent_id,2,$amount);
    }
    public function creditAgent($agent_id,$amount){
        return $this->updateAgentCredit($agent_id,1,$amount);
    }
    public function getAgents($issuer_id, $user_type_id, $user_id = null, $offset = 0, $count = 0){
          return $this->request(ServiceConstant::URL_ADD_GET_USERS,
            array(
                'issuer_id' =>  $issuer_id,
                'user_type_id' =>  $user_type_id,
                
                ),
                self::HTTP_GET
            );
    }
    
    /**
     * @return array|mixed|string
     */
    public function getAllAgents()
    {
        return $this->request(
            "agent/getAll/",
            array('user_type_id' => 4),
            self::HTTP_GET
        );
    }

} 