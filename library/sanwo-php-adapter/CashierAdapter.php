<?php


namespace SanwoPHPAdapter;


use SanwoPHPAdapter\Globals\ServiceConstant;

class CashierAdapter extends BaseAdapter {

    public function addCashier($merchant_id, $email,$telephone, $password,$firstname,$lastname,$middlename,$address,$gender){
        return $this->request(ServiceConstant::URL_ADD_CASHIER,array(
            'id'=>$merchant_id,
            'email'=>$email,
            'telephone'=>$telephone,
            'password'=>$password,
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'middlename'=>$middlename,
            'address'=>$address,
            'gender'=>$gender,
        ),self::HTTP_POST);
    }

    public function getAllCashier($merchant_id){
        return $this->request(ServiceConstant::URL_GET_ALL_CASHIER,array(
            'id'=>$merchant_id
        ),self::HTTP_GET);
    }
    public function getCashier($id){
        return false;
    }

} 