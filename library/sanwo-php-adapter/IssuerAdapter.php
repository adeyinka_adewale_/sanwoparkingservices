<?php

namespace SanwoPHPAdapter;
use SanwoPHPAdapter\BaseAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;


class IssuerAdapter extends BaseAdapter{

    public function createIssuer($name,$address){
        return $this->request(ServiceConstant::URL_ADD_ISSUER,array(
            'name' => $name,
            'address' => $address
        ),self::HTTP_POST);
    }
    /*
     * $id = $this->request->getPost('id');
        $email = $this->request->getPost('email', 'email');
        $password = $this->request->getPost('password');
        $phone_number = $this->request->getPost('telephone');
        $firstname = $this->request->getPost('firstname');
        $lastname = $this->request->getPost('lastname');
        $middlename = $this->request->getPost('middlename'); //optional
        $address = $this->request->getPost('address');  //optional
        $gender = $this->request->getPost('gender');
     */
    public function createIssuerUser($id,$password,$email,$phone,$firstname,$lastname,$middlename,$address,$gender,$user_type_id=9){
        $payload = array(
            'id' => $id,
            'email' => $email,
            'password' => $password,
            'telephone' => $phone,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'middlename' => $middlename,
            'gender' => $gender,
            'address' => $address,
            'user_type_id' => $user_type_id
        );
        return $this->request(ServiceConstant::URL_ADD_ISSUER_USER,$payload,self::HTTP_POST);
    }
    public function getAll()
    {
        return $this->request(
            ServiceConstant::URL_ADD_GET_All,
            array(),
            self::HTTP_GET
        );
    }
    public function getAllIssuerStaff($issuer_id)
    {
        return $this->request(
            ServiceConstant::URL_ADD_GET_USERS,
            array('user_type_id'=>9,'issuer_id' => $issuer_id),
            self::HTTP_GET
        );
    }
    public function changeStatus($id,$status){
        return $this->request(ServiceConstant::URL_CHANGE_STATUS,array(
            'id' => $id,
            'status' => $status
        ),self::HTTP_POST);
    }

    public function getIssuerUsers($issuer_id, $user_type_id, $offset = 0, $count = 0){
        return $this->request(ServiceConstant::URL_ADD_GET_USERS,
            array(
                'issuer_id' =>  $issuer_id,
                'user_type_id' =>  $user_type_id,
                'offset' =>  $offset,
                '$count' => $count
                
                ),
                self::HTTP_POST
            );

    }
}