<?php


namespace SanwoPHPAdapter;


use SanwoPHPAdapter\Globals\ServiceConstant;

class MerchantAdapter extends BaseAdapter
{

    /**
     * @author Adegoke Obasa
     * @param $name
     * @param $address
     * @param $phone_number
     * @param $business_type
     * @param $charge_type
     * @param $charge
     * @param $account_number
     * @param $bank
     * @param $account_name
     * @param $created_by
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function addMerchant($name, $address, $phone_number, $business_type, $charge_type, $charge, $account_number, $bank, $account_name, $created_by, $email, $password)
    {
        return $this->request(
            "merchant/addMerchant/",
            array(
                'name' => $name,
                'address' => $address,
                'phone_number' => $phone_number,
                'business_type' => $business_type,
                'charge_type' => $charge_type,
                'charge' => $charge,
                'account_number' => $account_number,
                'bank' => $bank,
                'account_name' => $account_name,
                'created_by' => $created_by,
                'email' => $email,
                'password' => $password,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $status
     * @return array|mixed|string
     */
    public function changeStatus($id, $status)
    {
        return $this->request(
            "merchant/changeStatus/",
            array(
                'id' => $id,
                'status' => $status,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @return array|mixed|string
     */
    public function remove($id)
    {
        return $this->request(
            "merchant/remove/",
            array(
                'id' => $id
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $name
     * @param $address
     * @param $phone_number
     * @param $business_type
     * @param $charge_type
     * @param $charge
     * @param $account_number
     * @param $bank
     * @param $account_name
     * @return array|mixed|string
     */
    public function edit($id, $name, $address, $phone_number, $business_type, $charge_type, $charge, $account_number, $bank, $account_name)
    {
        return $this->request(
            "merchant/edit/",
            array(
                'id' => $id,
                'name' => $name,
                'address' => $address,
                'phone_number' => $phone_number,
                'business_type' => $business_type,
                'charge_type' => $charge_type,
                'charge' => $charge,
                'account_number' => $account_number,
                'bank' => $bank,
                'account_name' => $account_name,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @return array|mixed|string
     */
    public function get($id)
    {
        return $this->request(
            "merchant/get/",
            array(
                'id' => $id
            ),
            self::HTTP_GET
        );
    }

    /**
     * @return array|mixed|string
     */
    public function getAll()
    {
        return $this->request(
            "merchant/getAll/",
            array(),
            self::HTTP_GET
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function addCashier($id, $email, $password,$telephone, $firstname, $lastname, $gender)
    {
        return $this->request(
            "merchant/addCashier/",
            array(
                'id' => $id,
                'email' => $email,
                'password' => $password,
                'telephone' => $telephone,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'gender' => $gender,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function addManager($id, $email, $password)
    {
        return $this->request(
            "merchant/addManager/",
            array(
                'id' => $id,
                'email' => $email,
                'password' => $password,
            ),
            self::HTTP_POST
        );
    }
    public function getMerchantDevices($merchant_id){
        return $this->request(
            ServiceConstant::URL_GET_MERCHANT_DEVICES,
            array(
                'id' => $merchant_id
            ),
            self::HTTP_GET
        );
    }
    public function getAllDevices($offset, $count){
        return $this->request(
            "device/getAll/",
            array(
                'offset' => $offset,
                'count' => $count
            ),
            self::HTTP_GET
        );
    }

   public function merchantSummary($offset = 0, $count = 20)
    {
        return $this->request(
            "report/merchantSummary",
            array(
                'count' => $count,
                'offset' => $offset
            ),
            self::HTTP_GET
        );
    }
} 