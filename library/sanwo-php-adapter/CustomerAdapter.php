<?php


namespace SanwoPHPAdapter;


class CustomerAdapter extends BaseAdapter {

    /**
     * @author Adegoke Obasa
     * @param $serial_number
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function register($serial_number, $email, $password)
    {
        return $this->request(
            "customer/register/",
            array(
                'serial_number' => $serial_number,
                'email' => $email,
                'password' => $password,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $email
     * @param $serial_number
     * @return array|mixed|string
     */
    public function addCard($email, $serial_number)
    {
        return $this->request(
            "customer/addCard/",
            array(
                'serial_number' => $serial_number,
                'email' => $email
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $user_id
     * @param $offset
     * @param $count
     * @return array|mixed|string
     */
    public function getCards($user_id, $offset, $count,$device_type_id=null)
    {
        if($device_type_id != null){
            return $this->request(
                "customer/getCards/".$user_id."?offset=".$offset."&device_type_id=".$device_type_id."&with_owner=1&count=".$count,
                array(),
                self::HTTP_GET
            );
        }
        return $this->request(
            "customer/getCards/".$user_id."?offset=".$offset."&count=".$count,
            array(),
            self::HTTP_GET
        );
    }
    public function getBySerial($serial_number)
    {
        return $this->request(
            "customer/getBySerial/?serial_number=".$serial_number,
            array(),
            self::HTTP_GET
        );
    }
    public function editCustomer($id,$firstname,$lastname,$middlename,$address,$phone_number,$gender){

        return $this->request(
            "profile/edit",
            [
                "id"=>$id,
                "firstname" => $firstname,
                "lastname" => $lastname,
                "middlename" => $middlename,
                "address" => $address,
                "phone_number"=> $phone_number,
                "gender" => $gender
            ],
            self::HTTP_POST
        );
    }

} 