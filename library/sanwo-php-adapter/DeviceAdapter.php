<?php


namespace SanwoPHPAdapter;

use SanwoPHPAdapter\Globals\ServiceConstant;


class DeviceAdapter extends BaseAdapter {

    public static $deviceTypes = [
        1 => 'AGENT',
        2 => 'CASHIER'
    ];

    /**
     * @author Adegoke Obasa
     * @param $device_type_id
     * @param $device_code
     * @param $address
     * @return array|mixed|string
     */
    public function addDevice($device_type_id, $device_code, $address)
    {
        return $this->request(
            "device/addDevice/",
            array(
                'device_type_id' => $device_type_id,
                'device_code' => $device_code,
                'address' => $address
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $status
     * @return array|mixed|string
     */
    public function changeStatus($id, $status)
    {
        return $this->request(
            "device/changeStatus/",
            array(
                'id' => $id,
                'status' => $status,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $status
     * @return array|mixed|string
     */
    public function remove($id, $status)
    {
        return $this->request(
            "device/remove/",
            array(
                'id' => $id
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $id
     * @param $device_type_id
     * @param $device_code
     * @param $address
     * @return array|mixed|string
     */
    public function edit($id, $device_type_id, $device_code, $address)
    {
        return $this->request(
            "device/edit/",
            array(
                'id' => $id,
                'device_type_id' => $device_type_id,
                'device_code' => $device_code,
                'address' => $address
            ),
            self::HTTP_POST
        );
    }

    /**
     * @param int $offset
     * @param int $count
     * @return array|mixed|string
     */
    public function getAll($offset = 0, $count = 25)
    {
        return $this->request(
            "device/getAll",
            array(
                'offset' => $offset,
                'count' => $count,
                'with_holder' => 1,
                'device_type_id' =>1
            ),
            self::HTTP_GET
        );
    }

    /**
     * @param $device_id
     * @param $merchant_id
     * @return array|mixed|string
     */
    public function assignDevice($device_id,$merchant_id){
        return $this->request(
            ServiceConstant::URL_ASSIGN_DEVICE,
            array(
                'merchant_id' => $merchant_id,
                'device_id' => $device_id
            ),
            self::HTTP_POST
        );
    }

    public function linkDevice($assignee_id,$device_type_id,$device_code,$address){
        return $this->request(
            "device/linkdevice/",
            array(
                'assignee_type' => $device_type_id,
                'id' => $assignee_id,
                'device_code' => $device_code,
                'address' => $address
            ),
            self::HTTP_POST
        );
    }
    public function getDevice($id){
        return $this->request(
            ServiceConstant::URL_GET_AGENT_DEVICES.$id,
            array(),
            self::HTTP_GET
        );
    }

} 