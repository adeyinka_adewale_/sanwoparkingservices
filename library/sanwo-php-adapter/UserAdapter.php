<?php
namespace SanwoPHPAdapter;
use SanwoPHPAdapter\BaseAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;

/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 3:41 AM
 */

class UserAdapter extends BaseAdapter
{
    /**
     * @author Adegoke Obasa
     * @param $identifier
     * @param $password
     * @return array|mixed|string
     */
    public function login($identifier, $password)
    {
        return $this->request(
            "user/login",
            array(
                'identifier' => $identifier,
                'password' => $password
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @return array|mixed|string
     */
    public function logout()
    {
        return $this->request(
            "user/login",
            array(),
            self::HTTP_GET
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $password
     * @return array|mixed|string
     */
    public function changePassword($password)
    {
        return $this->request(
            "user/changePassword",
            array(
                'password' => $password
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $email
     * @return array|mixed|string
     */
    public function getByEmail($email)
    {
        return $this->request(
            "user/getByEmail",
            array(
                'email' => $email
            ),
            self::HTTP_GET
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $user_id
     * @return array|mixed|strings
     */
    public function get($user_id)
    {
        return $this->request(
            "user/get/" . $user_id,
            array(),
            self::HTTP_GET
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $email
     * @param int $offset
     * @param int $count
     * @return array|mixed|string
     */
    public function getAll($offset = 0, $count = 25)
    {
        return $this->request(
            "user/getAll",
            array(
                'offset' => $offset,
                'count' => $count,
            ),
            self::HTTP_GET
        );
    }

    /**
     * @param $user_type_id
     * @param int $offset
     * @param int $count
     * @return array|mixed|string
     */
    public function getAllByType($user_type_id, $offset = 0, $count = 25)
    {
        return $this->request(
            "user/getAll",
            array(
                'user_type_id' => $user_type_id,
                'offset' => $offset,
                'count' => $count,
            ),
            self::HTTP_GET
        );
    }

    public function createAgent($email,$telephone,$password,$user_type_id,$status){
        
        return $this->request(ServiceConstant::URL_CREATE_AGENT,array(
            'email' => $email,
            'telephone'=>$telephone,
            'password'=>$password,
            'user_type_id'=>$user_type_id,
            'status'=>$status
        ),self::HTTP_POST);

    }
    public function createCustomer($card_serial,$email,$telephone,$firstname,$lastname,$middlename,$gender,$address,$user_type_id,$status){
        return $this->request(ServiceConstant::URL_CREATE_CUSTOMER,array(
            'serial_number' => $card_serial,
            'email' => $email,
            'telephone'=>$telephone,
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'middlename'=>$middlename,
            'gender'=>$gender,
            'address'=>$address,
            'user_type_id'=>$user_type_id,
            'status'=>$status
        ),self::HTTP_POST);
    }
}