<?php
/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 4:45 AM
 */

namespace SanwoPHPAdapter;

class ProfileAdapter extends BaseAdapter
{
    public function edit($firstname, $lastname, $phone_number, $gender, $middlename = null, $address = null)
    {
        return $this->request(
            "profile/edit",
            array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'middlename' => $middlename,
                'address' => $address,
                'phone_number' => $phone_number,
                'gender' => $gender,
            ),
            self::HTTP_POST
        );
    }
}