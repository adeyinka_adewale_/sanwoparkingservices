<?php

class Config {
    const ACCOUNT_STATUS_ACTIVE = 1;
    const ACCOUNT_STATUS_INACTIVE = 0;
    const ACCOUNT_STATUS_DEACTIVATED = 2;
    const ACCOUNT_STATUS_DELETED = 3;

    const RESPONSE_CODE_SESSION_INVALID = 6;
    const RESPONSE_CODE_RE_LOGIN = 7;

    const ACTIVATION_STATUS_PENDING = 4;
    const ACTIVATION_STATUS_ACTIVATED = 5;

    const ResponseStatusError = 'error';
    const ResponseStatusSuccess = 'success';


    const INSTITUTION_STAFF = 4;
    const ADMIN_STAFF = 3;
    const INSTITUTION_ADMIN = 2;
    const ADMIN = 1;
}