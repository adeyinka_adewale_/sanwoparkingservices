<?php

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;

use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Config\Adapter\Ini as ConfigIni;

require realpath('..') . '/app/config/config.php';


require realpath('..') . "/app/vendor/autoload.php";


try {

    // Register an autoloader
    $loader = new Loader();
    // Read the configuration
    $config = new ConfigIni(realpath('..') . '/app/config/config.ini');

    $loader->registerClasses(

        array(


        'CurlAgent' => '../library/sanwo-php-adapter/util/CurlAgent.php',
        )

    );
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/',
        '../vendor/autoload.php',
        '../library/'

    ))->register();

    // Create a DI
    $di = new FactoryDefault();

    // Setup the view component
    $di->set('view', function () {
        $view = new View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    // Setup a base URI so that all generated URIs include the "tutorial" folder
    $di->set('url', function () {
        $url = new UrlProvider();
        $url->setBaseUri('/SanwoParkingService/');
        return $url;
    });

    //setup db services
    $di->set('db', function () {
        return new DbAdapter(array(
            "host" => "localhost",
            "username" => "root",
            "password" => "",
            "dbname" => "sanwo_parking"
        ));
    });

    // Handle the request
    $application = new Application($di);

    $response = $application->response;
    $response->setHeader('Access-Control-Allow-Origin', '*');
    $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
    $response->sendHeaders();

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
}